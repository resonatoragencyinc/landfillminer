function CLevelSettings(){

    var _aNuggetPos;
    var _aNuggetInfo;
    var _aMalusPos;
    var _aLevelTarget;

    this._init = function(){
        _aNuggetPos = new Array();
        _aNuggetInfo = new Array();
        _aMalusPos = new Array();

        // randomize helper
        function getRandom(min, max) {
          return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        // randomize x & y
        function randomX() {
          return getRandom(300, 1200);
        }

        function randomY(){
          return getRandom(250, 650);
        }

        function setLocation() {
          // var malusArray = [];
          var position = {}
          position['x'] = randomX();
          position['y'] = randomY();
          // malusArray.push(position);
          // return malusArray;
          return position;
        }

        // randomize nugglet type and scale
        function getRandomType() {
          return Math.floor((Math.random() * 10) + 3);
        }

        function getRandomScale() {
          return parseFloat((Math.random() * 0.5 + 0.5).toFixed(1));
        }

        function setInfo() {
          var info = {};
          info['type'] = getRandomType();
          info['scale'] = getRandomScale();
          return info;
        }

        var initArray = new Array(7).fill("");

        for (var i = 0; i < 4; i++) {
          var aNuggets = initArray.map(function() {
            return setLocation();
          });
          _aNuggetPos.push(aNuggets);
          var aInfos = aNuggets.map(function(){
            return setInfo();
          });

          _aNuggetInfo.push(aInfos);
          var aMalus = [setLocation()];
          _aMalusPos.push(aMalus);
        }

        // //LEVEL 1
        // var aNuggets = initArray.map(function() {
        //   return setLocation();
        // });
        // _aNuggetPos.push(aNuggets);
        // var aInfos = aNuggets.map(function(){
        //   return setInfo();
        // });
        //
        // _aNuggetInfo.push(aInfos);
        // var aMalus = [setLocation()];
        // _aMalusPos.push(aMalus);
        //
        // //LEVEL 2
        // aNuggets = [{x:1048,y:580},{x:368,y:350},{x:738,y:380},{x:438,y:550},{x:1248,y:600},{x:868,y:680},{x:1168,y:370}];
        // _aNuggetPos.push(aNuggets);
        // aInfos = aNuggets.map(function(){
        //   return setInfo();
        // });
        // _aNuggetInfo.push(aInfos);
        // aMalus = [setLocation()];
        // _aMalusPos.push(aMalus);
        //
        // //LEVEL 3
        // aNuggets = [{x:948,y:350},{x:768,y:300},{x:538,y:390},{x:338,y:450},{x:488,y:550},{x:1188,y:500},{x:1038,y:650}];
        // _aNuggetPos.push(aNuggets);
        // aInfos = aNuggets.map(function(item){
        //   return setInfo();
        // });
        // _aNuggetInfo.push(aInfos);
        // aMalus = [setLocation()];
        // _aMalusPos.push(aMalus);
        //
        // //LEVEL 4
        // aNuggets = [{x:748,y:550},{x:568,y:600},{x:938,y:390},{x:1038,y:450},{x:1188,y:550},{x:1238,y:300},{x:1038,y:650}];
        // _aNuggetPos.push(aNuggets);
        // aInfos = aNuggets.map(function(item){
        //   return setInfo();
        // });
        // _aNuggetInfo.push(aInfos);
        // aMalus = [setLocation()];
        // _aMalusPos.push(aMalus);


        //INIT ALL LEVEL TARGET
        _aLevelTarget = new Array();

        //TARGET LEVEL 1
        _aLevelTarget.push(2000);
        //TARGET LEVEL 2
        _aLevelTarget.push(7000);
        //TARGET LEVEL 3
        _aLevelTarget.push(11000);
        //TARGET LEVEL 4
        _aLevelTarget.push(15000);
    };

    this.getNuggetPosInLevel = function(iLevel){
        return _aNuggetPos[iLevel-1];
    };

    this.getNuggetInfoInLevel = function(iLevel){
        return _aNuggetInfo[iLevel-1];
    };

    this.getMalusPosInLevel = function(iLevel){
        return _aMalusPos[iLevel-1];
    };

    this.getLevelTarget = function(iLevel){
        return _aLevelTarget[iLevel-1];
    };

    this.getNumLevels = function(){
        return _aLevelTarget.length;
    };

    this._init();
}
